<?php
define('WP_POST_REVISIONS', 5); // Added by WP Disable
/**
 * La configuration de base de votre installation WordPress.
 *
 * Le script de création wp-config.php utilise ce fichier lors de l'installation.
 * Vous n'avez pas à utiliser l'interface web, vous pouvez directement
 * renommer ce fichier en "wp-config.php" et remplir les variables à la main.
 * 
 * Ce fichier contient les configurations suivantes :
 * 
 * * réglages MySQL ;
 * * clefs secrètes ;
 * * préfixe de tables de la base de données ;
 * * ABSPATH.
 * 
 * @link https://codex.wordpress.org/Editing_wp-config.php 
 * 
 * @package WordPress
 */

// ** Réglages MySQL - Votre hébergeur doit vous fournir ces informations. ** //
/** Nom de la base de données de WordPress. */
define('DB_NAME', 'epicosm');

/** Utilisateur de la base de données MySQL. */
define('DB_USER', 'root');

/** Mot de passe de la base de données MySQL. */
define('DB_PASSWORD', '');

/** Adresse de l'hébergement MySQL. */
define('DB_HOST', 'localhost');

/** Jeu de caractères à utiliser par la base de données lors de la création des tables. */
define('DB_CHARSET', 'utf8');

/** Type de collation de la base de données. 
  * N'y touchez que si vous savez ce que vous faites. 
  */
define('DB_COLLATE', '');

/**#@+
 * Clefs uniques d'authentification et salage.
 *
 * Remplacez les valeurs par défaut par des phrases uniques !
 * Vous pouvez générer des phrases aléatoires en utilisant 
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ le service de clefs secrètes de WordPress.org}.
 * Vous pouvez modifier ces phrases à n'importe quel moment, afin d'invalider tous les cookies existants.
 * Cela forcera également tous les utilisateurs à se reconnecter.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '@)}i6)OEi5+34y=^t72{G|`b~_-wM4`9^S,xzMZHb;NVmY0aV0OJj{m(g)!{ARP)');
define('SECURE_AUTH_KEY',  'F-o>5.iC _NmkV)KH9#U(HJw<Av|MwdLn!GOS>s:Z9V5`fgb79Y>x+p?*CMHf4wu');
define('LOGGED_IN_KEY',    ',1f`7u+i$tU*,x+QHO>?4#hp*iN Xom|Y;,_].]<@H:7+@yLT3]*d5=$~ZR-c{J,');
define('NONCE_KEY',        'tTA`Wf0i_vmM,bN-&*QdN>UNy?H3F9<t^u #W!$MD;tZQnXcc)Tza]:8wG|ck2PD');
define('AUTH_SALT',        'E}0]mUTKyK|zsT#JOHv6g)N)ws;PN|Sd}.;Af*Zm}p+{3=*w9r|b(|7C-cH>[C4s');
define('SECURE_AUTH_SALT', 'p(,EEwsB0<t)jP>ek&>m$W=4l,1cGU(ORL]wMu|3Fi#|eg9yl>^d:8$slyw3,k61');
define('LOGGED_IN_SALT',   '3w+^[t|%_g0k AM;@3Yx]+CT1}W!+hdd$>CB+*a;?xgFki=-IR,ZR_` 3J{7Q9jS');
define('NONCE_SALT',       'F.%.6YSkvB*(wjOl3?H%u$H(2e?0+Sk65qy:(pw9w-%:{sB.YTw+nbeKk>J21miF');
/**#@-*/

/**
 * Préfixe de base de données pour les tables de WordPress.
 *
 * Vous pouvez installer plusieurs WordPress sur une seule base de données
 * si vous leur donnez chacune un préfixe unique. 
 * N'utilisez que des chiffres, des lettres non-accentuées, et des caractères soulignés!
 */
$table_prefix  = 'wpepc_';

/** 
 * Pour les développeurs : le mode déboguage de WordPress.
 * 
 * En passant la valeur suivante à "true", vous activez l'affichage des
 * notifications d'erreurs pendant votre essais.
 * Il est fortemment recommandé que les développeurs d'extensions et
 * de thèmes se servent de WP_DEBUG dans leur environnement de 
 * développement.
 * 
 * Pour obtenir plus d'information sur les constantes 
 * qui peuvent être utilisée pour le déboguage, consultez le Codex.
 * 
 * @link https://codex.wordpress.org/Debugging_in_WordPress 
 */ 
define('WP_DEBUG', true); 
define('WP_DEBUG_DISPLAY', true); 
define('WP_DEBUG_LOG', true); 
define( 'AUTOSAVE_INTERVAL', 120 ); // Seconds
define('DISABLE_WP_CRON', true);

/* C'est tout, ne touchez pas à ce qui suit ! Bon blogging ! */

/** Chemin absolu vers le dossier de WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Réglage des variables de WordPress et de ses fichiers inclus. */
require_once(ABSPATH . 'wp-settings.php');