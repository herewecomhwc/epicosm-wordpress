<?php
/**
 * The cookie scanning functionality of the plugin.
 *
 * @link       http://cookielawinfo.com/
 * @since      2.1.5
 *
 * @package    Cookie_Law_Info
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
include( plugin_dir_path( __FILE__ ).'classes/class-cookie-scanner-ajax.php');

class Cookie_Law_Info_Cookie_Scaner extends Cookie_Law_Info_Cookieyes
{
	
	public $main_tb='cli_cookie_scan';
	public $url_tb='cli_cookie_scan_url';
	public $cookies_tb='cli_cookie_scan_cookies';
	public $category_table = 'cli_cookie_scan_categories';
	public $not_keep_records=true;
	public $scan_page_mxdata; //maximum url per request for scanning //!important do not give value more than 5
	public $fetch_page_mxdata=100; //take pages
	public $last_query;
	protected $ckyes_scan_id;
	protected $ckyes_scan_token;
	
	public function __construct()
	{		
		/* creating necessary tables for cookie scaner  */
        register_activation_hook(CLI_PLUGIN_FILENAME,array($this,'activator'));
        $this->status_labels=array(
			0	=>	'',
			1	=>	__('Incomplete','webtoffee-gdpr-cookie-consent'),
			2	=>	__('Completed','webtoffee-gdpr-cookie-consent'),
			3	=>	__('Stopped','webtoffee-gdpr-cookie-consent'),
			4	=>	__('Failed','webtoffee-gdpr-cookie-consent'),
		);
        add_action('admin_init',array( $this,'export_result'));
		add_action( 'admin_menu', array($this,'add_admin_pages'));
		
		$url_per_request=get_option('cli_cs_url_per_request');
        if(!$url_per_request)
        {
            $url_per_request=5;
        }
        $this->scan_page_mxdata=$url_per_request;

		add_action('wt_cli_cookie_scan_notices',array( $this,'scanner_notices'));

		add_action('rest_api_init', function () {
			register_rest_route('cookieyes/v1', '/fetch_results', array(
				'methods' => 'POST',
				'callback' => array($this, 'fetch_scan_result'),
				'permission_callback' => '__return_true'
			));
		});
	}
	
	/*
    * returning labels of status
    */
	public function getStatusText($status)
	{
		return isset($this->status_labels[$status]) ? $this->status_labels[$status] : __('Unknown','webtoffee-gdpr-cookie-consent');
	}

	/*
    * export to csv
    */
	public function export_result()
	{
		if(isset($_GET['cli_scan_export']) && (int) $_GET['cli_scan_export']>0 && check_admin_referer('cli_cookie_scaner', 'cli_cookie_scaner') && current_user_can('manage_options')) 
		{	
			//cookie export class
            include( plugin_dir_path( __FILE__ ).'classes/class-cookie-export.php');
            $cookie_serve_export=new Cookie_Law_Info_Cookie_Export();
            $cookie_serve_export->do_export($_GET['cli_scan_export'],$this);
			exit();
		}
	}

	/*
    *called on activation
    */
    public function activator()
    {
        global $wpdb;
        require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );       
        if(is_multisite()) 
        {
            // Get all blogs in the network and activate plugin on each one
            $blog_ids = $wpdb->get_col( "SELECT blog_id FROM $wpdb->blogs" );
            foreach($blog_ids as $blog_id) 
            {
                switch_to_blog( $blog_id );
                $this->install_tables();
		        if(!get_option('cli_cs_url_per_request'))
		        {
		            update_option('cli_cs_url_per_request',5);
		        }
                restore_current_blog();
            }
        }
        else 
        {
            $this->install_tables();
            if(!get_option('cli_cs_url_per_request'))
	        {
	            update_option('cli_cs_url_per_request',5);
	        }
        }
    }

    /*
    * Install necessary tables
    */
    public function install_tables()
    {
		global $wpdb;
		
		$charset_collate = $wpdb->get_charset_collate();
		
        //creating main table ========================
        $table_name=$wpdb->prefix.$this->main_tb;
        $search_query = "SHOW TABLES LIKE '%".$table_name."%'";
        if(!$wpdb->get_results($search_query,ARRAY_N)) 
        {           
            $create_table_sql= "CREATE TABLE `$table_name`(
			    `id_cli_cookie_scan` INT NOT NULL AUTO_INCREMENT,
			    `status` INT NOT NULL DEFAULT '0',
			    `created_at` INT NOT NULL DEFAULT '0',
			    `total_url` INT NOT NULL DEFAULT '0',
			    `total_cookies` INT NOT NULL DEFAULT '0',
			    `current_action` VARCHAR(50) NOT NULL,
			    `current_offset` INT NOT NULL DEFAULT '0',
			    PRIMARY KEY(`id_cli_cookie_scan`)
			);";
            dbDelta($create_table_sql);
        }
        //creating main table ========================


        //creating url table ========================
        $table_name=$wpdb->prefix.$this->url_tb;
        $search_query = "SHOW TABLES LIKE '%".$table_name."%'";
        if(!$wpdb->get_results($search_query,ARRAY_N)) 
        {           
            $create_table_sql= "CREATE TABLE `$table_name`(
			    `id_cli_cookie_scan_url` INT NOT NULL AUTO_INCREMENT,
			    `id_cli_cookie_scan` INT NOT NULL DEFAULT '0',
			    `url` TEXT NOT NULL,
			    `scanned` INT NOT NULL DEFAULT '0',
			    `total_cookies` INT NOT NULL DEFAULT '0',
			    PRIMARY KEY(`id_cli_cookie_scan_url`)
			);";
            dbDelta($create_table_sql);
		}
		
        //creating url table ========================

        //creating cookies table ========================
        $table_name=$wpdb->prefix.$this->cookies_tb;
        $search_query = "SHOW TABLES LIKE '%".$table_name."%'";
        if(!$wpdb->get_results($search_query,ARRAY_N)) 
        {           
            $create_table_sql= "CREATE TABLE `$table_name`(
			    `id_cli_cookie_scan_cookies` INT NOT NULL AUTO_INCREMENT,
			    `id_cli_cookie_scan` INT NOT NULL DEFAULT '0',
			    `id_cli_cookie_scan_url` INT NOT NULL DEFAULT '0',
			    `cookie_id` VARCHAR(100) NOT NULL,
			    `expiry` VARCHAR(255) NOT NULL,
			    `type` VARCHAR(255) NOT NULL,
			    `category` VARCHAR(255) NOT NULL,
			    PRIMARY KEY(`id_cli_cookie_scan_cookies`),
			    UNIQUE `cookie` (`id_cli_cookie_scan`, `cookie_id`)
			)";
			$this->insert_scanner_tables( $create_table_sql, $charset_collate );
		}
		//creating cookies table ========================

		 //creating categories table ========================
		 $table_name=$wpdb->prefix.$this->category_table;
		 $search_query = "SHOW TABLES LIKE '%".$table_name."%'";
		 if(!$wpdb->get_results($search_query,ARRAY_N)) 
		 {           
			 $create_table_sql= "CREATE TABLE `$table_name`(
				 `id_cli_cookie_category` INT NOT NULL AUTO_INCREMENT,
				 `cli_cookie_category_name` VARCHAR(100) NOT NULL,
				 `cli_cookie_category_description` TEXT  NULL,
				 PRIMARY KEY(`id_cli_cookie_category`),
				 UNIQUE `cookie` (`cli_cookie_category_name`)
			 )";
			 $this->insert_scanner_tables( $create_table_sql, $charset_collate );
		 }
		 //creating cookies table ========================
        $this->update_tables();
	}
	/**
	* Error handling of scanner table creation
	*
	* @since  2.3.1
	* @access private
	* @throws Exception Error message.
	* @param  string
	*/
	private function insert_scanner_tables( $sql, $prop = '', $status = 0 ) {
		
		global $wpdb;
		dbDelta( $sql.' '.$prop );
		if( $wpdb->last_error ) {
			$status++;
			if( $status === 1) {
				$prop = '';
			} else if( $status === 2) {
				$prop = 'ENGINE = MyISAM CHARACTER SET utf8 COLLATE utf8_general_ci';
			} else {
				return true;
			}
			$this->insert_scanner_tables( $sql, $prop, $status);
		} else {
			return true;
		}
	}
	/*
    * @since 2.1.9
    * update the table
    */
    private function update_tables()
    {
    	global $wpdb;
    	//Cookie table =======
        //`description` column
		$table_name=$wpdb->prefix.$this->cookies_tb;
		$cat_table=$wpdb->prefix.$this->category_table;
        $search_query = "SHOW COLUMNS FROM `$table_name` LIKE 'description'";
        if(!$wpdb->get_results($search_query,ARRAY_N)) 
        {
        	$wpdb->query("ALTER TABLE `$table_name` ADD `description` TEXT NULL DEFAULT '' AFTER `category`");
		}
		// category_id` column
		$search_query = "SHOW COLUMNS FROM `$table_name` LIKE 'category_id'";
        if(!$wpdb->get_results($search_query,ARRAY_N)) 
        {
			$wpdb->query("ALTER TABLE `$table_name` ADD `category_id` INT NOT NULL  AFTER `category`");
			$wpdb->query("ALTER TABLE `$table_name` ADD CONSTRAINT FOREIGN KEY (`category_id`) REFERENCES `$cat_table` (`id_cli_cookie_category`)");
			
		}
		
	}
	
    /*
    * checking necessary tables are installed
    */
    protected function check_tables()
    {
    	global $wpdb;
    	$out=true;
    	//checking main table ========================
        $table_name=$wpdb->prefix.$this->main_tb;
        $search_query = "SHOW TABLES LIKE '%".$table_name."%'";
        if(!$wpdb->get_results($search_query,ARRAY_N)) 
        {           
            $out=false;
        }

        //checking url table ========================
        $table_name=$wpdb->prefix.$this->url_tb;
        $search_query = "SHOW TABLES LIKE '%".$table_name."%'";
        if(!$wpdb->get_results($search_query,ARRAY_N)) 
        {           
            $out=false;
        }

        //checking cookies table ========================
        $table_name=$wpdb->prefix.$this->cookies_tb;
        $search_query = "SHOW TABLES LIKE '%".$table_name."%'";
        if(!$wpdb->get_results($search_query,ARRAY_N)) 
        {           
            $out=false;
        }
        return $out;
    }

	/**
	 * Add administration menus
	 *
	 * @since 2.1.5
	 **/
	public function add_admin_pages() 
	{
        add_submenu_page(
			'edit.php?post_type='.CLI_POST_TYPE,
			__('Cookie Scanner','webtoffee-gdpr-cookie-consent'),
			__('Cookie Scanner','webtoffee-gdpr-cookie-consent'),
			'manage_options',
			'cookie-law-info-cookie-scaner',
			array($this, 'cookie_scaner_page')
		);		
	}

	/*
	*
	* Scaner page (Admin page)
	*/
	public function cookie_scaner_page()
	{	
		$plugin_help_url = get_admin_url(null, 'edit.php?post_type=' . CLI_POST_TYPE . '&page=cookie-law-info#cookie-law-info-advanced');
		$cookie_list=self::get_cookie_list();
		wp_enqueue_script('cookielawinfo_cookie_scaner',plugin_dir_url( __FILE__ ).'assets/js/cookie-scaner.js',array(),CLI_VERSION);
		$scan_page_url=admin_url('edit.php?post_type='.CLI_POST_TYPE.'&page=cookie-law-info-cookie-scaner');
		$result_page_url=$scan_page_url.'&scan_result';
		$export_page_url=$scan_page_url.'&cli_scan_export=';
		$import_page_url=$scan_page_url.'&cli_cookie_import=';
		$last_scan=$this->get_last_scan();
		$params = array(
	        'nonces' => array(
	            'cli_cookie_scaner' => wp_create_nonce('cli_cookie_scaner'),
			),
			
	        'ajax_url' => admin_url('admin-ajax.php'),
	        'scan_page_url'=>$scan_page_url,
	        'result_page_url'=>$result_page_url,
	        'export_page_url'=>$export_page_url,
			'loading_gif'=>plugin_dir_url(__FILE__).'assets/images/loading.gif',
			'scan_status'	=>	( $this->check_scan_status() === 1  ? true : false ),
	        'labels'=>array(
	        	'scanned'=>__('Scanned','webtoffee-gdpr-cookie-consent'),
				'finished'=>__('Scanning completed.','webtoffee-gdpr-cookie-consent'),
				'import_finished'=>__('Added to cookie list.','webtoffee-gdpr-cookie-consent'),
				'retrying'=> sprintf( wp_kses( __( 'Unable to connect. Try setting the URL per scan request to 1 or 2 under advanced settings tab <a href="%s">click here.</a>', 'webtoffee-gdpr-cookie-consent' ), array(  'a' => array( 'href' => array(), 'target' => array() ) ) ), esc_url( $plugin_help_url ) ),
				'finding'=>__('Finding pages...','webtoffee-gdpr-cookie-consent'),
				'scanning'=>__('Scanning pages...','webtoffee-gdpr-cookie-consent'),
				'error'=>__('Error','webtoffee-gdpr-cookie-consent'),
				'stop'=>__('Stop','webtoffee-gdpr-cookie-consent'),
				'scan_again'=>__('Scan again','webtoffee-gdpr-cookie-consent'),
				'export'=>__('Download cookies as CSV','webtoffee-gdpr-cookie-consent'),
				'import'=>__('Add to cookie list','webtoffee-gdpr-cookie-consent'),
				'view_result'=>__('View scan result','webtoffee-gdpr-cookie-consent'),
				'import_options'=>__('Import options','webtoffee-gdpr-cookie-consent'),
				'replace_old'=>__('Replace old','webtoffee-gdpr-cookie-consent'),
				'merge'=>__('Merge','webtoffee-gdpr-cookie-consent'),
				'recommended'=>__('Recommended','webtoffee-gdpr-cookie-consent'),
				'append'=>__('Append','webtoffee-gdpr-cookie-consent'),
				'not_recommended'=>__('Not recommended','webtoffee-gdpr-cookie-consent'),
				'cancel'=>__('Cancel','webtoffee-gdpr-cookie-consent'),
				'start_import'=>__('Start import','webtoffee-gdpr-cookie-consent'),
				'importing'=>__('Importing....','webtoffee-gdpr-cookie-consent'),
				'refreshing'=>__('Refreshing....','webtoffee-gdpr-cookie-consent'),
				'reload_page'=>__('Error !!! Please reload the page to see cookie list.','webtoffee-gdpr-cookie-consent'),
				'stoping'=>__('Stopping...','webtoffee-gdpr-cookie-consent'),
				'scanning_stopped'=>__('Scanning stopped.','webtoffee-gdpr-cookie-consent'),
				'ru_sure'=>__('Are you sure?','webtoffee-gdpr-cookie-consent'),
				'success'=>__('Success','webtoffee-gdpr-cookie-consent'),
				'thankyou'=>__('Thank you','webtoffee-gdpr-cookie-consent'),
				'checking_api'=>__('Checking API','webtoffee-gdpr-cookie-consent'),
				'sending'=>__('Sending...','webtoffee-gdpr-cookie-consent'),
				'total_urls_scanned'=>__('Total URLs scanned','webtoffee-gdpr-cookie-consent'),
				'total_cookies_found'=>__('Total Cookies found','webtoffee-gdpr-cookie-consent'),
	        )
	    );
	    wp_localize_script('cookielawinfo_cookie_scaner','cookielawinfo_cookie_scaner',$params);
	    if(isset($_GET['scan_result']))
		{
			$scan_details=$this->get_last_scan();
			$scan_urls=array(
				'total'=>0,
				'data'=>array()
			);
			$scan_cookies=array(
				'total'=>0,
				'data'=>array()
			);
			if($scan_details && isset($scan_details['id_cli_cookie_scan']))
			{
				$scan_urls=$this->get_scan_urls($scan_details['id_cli_cookie_scan']);
				$scan_cookies=$this->get_scan_cookies($scan_details['id_cli_cookie_scan'],0,-1);
			}
			$view_file="scan-result.php";
		}else
		{
			$view_file="scan-cookies.php";
		}

		$localhost_arr = array(
		    '127.0.0.1',
		    '::1'
		);
		if(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) 
		{
		    $ip_address = $_SERVER['HTTP_X_FORWARDED_FOR'];
		} else {
		    $ip_address = $_SERVER['REMOTE_ADDR'];
		}
		$ip_address = apply_filters('wt_cli_change_ip_address',$ip_address );
	    if(!$this->check_tables() 
	    	|| version_compare(CLI_VERSION,'2.1.4')<=0 
	    	|| in_array($ip_address,$localhost_arr))
		{
			
			$error_message=__("Unable to load cookie scanner.","webtoffee-gdpr-cookie-consent");
			if(version_compare(CLI_VERSION,'2.1.4')<=0)
			{
				$error_message.=" ".__("Need `GDPR Cookie Consent` plugin version above 2.1.4","webtoffee-gdpr-cookie-consent");
			}
			if(in_array($ip_address,$localhost_arr))
			{
				$error_message.=" ".__("Scanning will not work on local server.","webtoffee-gdpr-cookie-consent");
			}
			// $view_file="unable-to-start.php";
		}
		include( plugin_dir_path( __FILE__ ).'views/'.$view_file);
	}

	/*
	*
	*	Create a DB entry for scanning
	*/
	protected function createScanEntry($total_url=0)
	{
		global $wpdb;

		//we are not planning to keep records of old scans
		if($this->not_keep_records)
		{
			$this->flushScanRecords();
		}

		$scan_table=$wpdb->prefix.$this->main_tb;
		$data_arr=array(
			'created_at'=>time(),
			'total_url'=>$total_url,
			'total_cookies'=>0,
			'current_action'=>'get_pages',
			'status'=>1
		);
		update_option('CLI_BYPASS',1);
		if($wpdb->insert($scan_table,$data_arr))
		{
			return $wpdb->insert_id;
		}else
		{
			return '0';
		}
	}

	/*
	*
	*	Update scanning status
	*/
	protected function updateScanEntry($data_arr,$scan_id)
	{
		global $wpdb;
		$scan_table=$wpdb->prefix.$this->main_tb;
		if($wpdb->update($scan_table,$data_arr,array('id_cli_cookie_scan'=>$scan_id)))
		{
			return true;
		}else
		{
			return false;
		}
	}

	/*
	*
	*	Insert URLs
	*/
	protected function insertUrl($scan_id,$permalink)
	{
		global $wpdb;
		$url_table=$wpdb->prefix.$this->url_tb;
		$data_arr=array(
        	'id_cli_cookie_scan'=>$scan_id,
        	'url'=>$permalink,
        	'scanned'=>0,
        	'total_cookies'=>0
        );
		$wpdb->insert($url_table,$data_arr);
	}

	/*
	*
	*	Insert Cookies
	*/
	protected function insertCookies($scan_id,$url_id,$url,$cookie_data,$out = array() )
	{
		global $wpdb;
		$url_table=$wpdb->prefix.$this->cookies_tb;
		$cat_table=$wpdb->prefix.$this->category_table;
		$sql="INSERT IGNORE INTO `$url_table` (`id_cli_cookie_scan`,`id_cli_cookie_scan_url`,`cookie_id`,`expiry`,`type`,`category`,`category_id`,`description`) VALUES ";
		$sql_arr=array();
		$out[]=$url;
		foreach($cookie_data as $cookies)
		{
			$cookie_id=trim($cookies['cookie_id']);
			$expiry=trim($cookies['duration']);
			$type=$cookies['type'];
			$category=$cookies['category'];
			$description=addslashes($cookies['description']);
			$category_id = $wpdb->get_var("SELECT `id_cli_cookie_category` FROM `$cat_table` WHERE `cli_cookie_category_name` = '$category'");
			$out[]='&nbsp;&nbsp;&nbsp;'.$cookie_id;
			$sql_arr[]="('$scan_id','$url_id','$cookie_id','$expiry','$type','$category','$category_id','$description')";
		}
		$sql=$sql.implode(",",$sql_arr);
		$wpdb->query($sql);
		return $out;
	}
	protected function insertCategories($cookie_data)
	{
		global $wpdb;
		$cat_table=$wpdb->prefix.$this->category_table;	
		$cat_arr=array();
		$cat_sql="INSERT IGNORE INTO `$cat_table` (`cli_cookie_category_name`,`cli_cookie_category_description`) VALUES ";
		foreach($cookie_data as $cookies)
		{
			$cat_description=addslashes($cookies['category_desc']);
			$category=$cookies['category'];	
			$cat_arr[]="('$category','$cat_description')";
		}
		$cat_sql=$cat_sql.implode(",",$cat_arr);
		$wpdb->query($cat_sql);
	}
	/*
    * @since 2.1.9
    * Insert categories
    */

	/*
	*
	*	Update scanned to URL
	*/
	protected function updateUrl($url_id_arr)
	{
		global $wpdb;
		$url_table=$wpdb->prefix.$this->url_tb;
		$sql="UPDATE `$url_table` SET `scanned`=1 WHERE id_cli_cookie_scan_url IN(".implode(",",$url_id_arr).")";
		$wpdb->query($sql);
	}
	
	/*
	*
	* Get last scan details
	*/
	protected function get_last_scan()
	{
		global $wpdb;
		$scan_table=$wpdb->prefix.$this->main_tb;
		$sql="SELECT * FROM `$scan_table` ORDER BY id_cli_cookie_scan DESC LIMIT 1";
		return $wpdb->get_row($sql,ARRAY_A);
	}

	/*
	*
	* URLs that are scanned
	*/
	public function get_scan_urls($scan_id,$offset=0,$limit=100)
	{
		global $wpdb;
		$out=array(
			'total'=>0,
			'data'=>array()
		);
		$url_table=$wpdb->prefix.$this->url_tb;
		$count_sql="SELECT COUNT(id_cli_cookie_scan_url) AS ttnum FROM $url_table WHERE id_cli_cookie_scan='$scan_id'";
		$count_arr=$wpdb->get_row($count_sql,ARRAY_A);
		if($count_arr){
			$out['total']=$count_arr['ttnum'];
		}

		$sql="SELECT * FROM $url_table WHERE id_cli_cookie_scan='$scan_id' ORDER BY id_cli_cookie_scan_url ASC LIMIT $offset,$limit";
		
		$data_arr=$wpdb->get_results($sql,ARRAY_A);
		if($data_arr){
			$out['data']=$data_arr;
		}
		return $out;
	}

	/*
	*
	* Cookies that are got while scanning
	*/
	public function get_scan_cookies($scan_id,$offset=0,$limit=100)
	{
		global $wpdb;
		$out=array(
			'total'=>0,
			'data'=>array()
		);
		$cookies_table=$wpdb->prefix.$this->cookies_tb;
		$url_table=$wpdb->prefix.$this->url_tb;
		$cat_table=$wpdb->prefix.$this->category_table;
		$count_sql="SELECT COUNT(id_cli_cookie_scan_cookies) AS ttnum FROM $cookies_table WHERE id_cli_cookie_scan='$scan_id'";
		$count_arr=$wpdb->get_row($count_sql,ARRAY_A);
		if($count_arr){
			$out['total']=$count_arr['ttnum'];
		}

		$sql="SELECT * FROM $cookies_table INNER JOIN $cat_table ON $cookies_table.category_id = $cat_table.id_cli_cookie_category INNER JOIN $url_table ON $cookies_table.id_cli_cookie_scan_url = $url_table.id_cli_cookie_scan_url WHERE $cookies_table.id_cli_cookie_scan='$scan_id' ORDER BY id_cli_cookie_scan_cookies ASC".($limit>0 ? " LIMIT $offset,$limit" : "");
		$data_arr=$wpdb->get_results($sql,ARRAY_A);
		if($data_arr){
			$out['data']=$data_arr;
		}
		return $out;
	}

	/*
	*
	* Taking existing cookie list (Manually created and Inserted via scanner)
	*/
	public static function get_cookie_list()
	{
		$args=array(
			'numberposts'=>-1,
			'post_type'=>CLI_POST_TYPE,
			'orderby'=>'ID',
			'order'=>'DESC'
		);
		return get_posts($args);
	}

	/*
	*
	* Delete all previous scan records
	*/
	public function flushScanRecords()
	{
		global $wpdb;
		$table_name=$wpdb->prefix.$this->main_tb; 
		$wpdb->query("TRUNCATE TABLE $table_name");
		$table_name=$wpdb->prefix.$this->url_tb;
		$wpdb->query("TRUNCATE TABLE $table_name");
		$table_name=$wpdb->prefix.$this->cookies_tb;
		$wpdb->query("TRUNCATE TABLE $table_name");
	}
	public function scanner_notices(){

		$license_url 		=	admin_url('edit.php?post_type=cookielawinfo&page=cookie-law-info#cookie-law-info-licence');
		if( $this->get_license_status() === false ) {
			$hide_scan_btn = true;
			echo '<div class="wt-cli-callout wt-cli-callout-alert"><p>'.__( 'Please activate your license in order to proceed with the scan.', 'webtoffee-gdpr-cookie-consent' ).' <a href="'.$license_url.'">'.__('Activate','webtoffee-gdpr-cookie-consent').'</a></p></div>';
		
		} else {

			if( $this->get_cookieyes_status() === false ) {
				$last_scan  = $this->get_last_scan();

				if( $last_scan ) {
					echo $this->get_cookieyes_scan_notice( true );
					echo $this->get_last_scan_info();
				} else {
					echo $this->get_cookieyes_scan_notice();
				}
			}
			else {
				if( $this->check_scan_status() === 1 ) {
					echo $this->get_scan_status_html();
				} else {
					echo $this->get_last_scan_info();
				}
			}
		}
	}
	public function get_license_activated_message(){
		
	}
	public function get_cookieyes_scan_notice( $existing = false ){

		$ckyes_link 			 = 	'https://www.cookieyes.com/';
		$ckyes_privacy_policy 	 = 	'https://www.cookieyes.com/privacy-policy';
		$ckyes_terms_conditions  = 	'https://www.cookieyes.com/terms-and-conditions/';
		$notice  = '<div class="wt-cli-callout wt-cli-callout-info" style="font-weight: 500;">';

		if( $existing === true  ) { // Existing user so show this notice
			$notice .= '<p>'.sprintf( wp_kses( __( 'GDPR Cookie Consent now uses <a href="%s" target="_blank">CookieYes</a> to bring you enhanced scanning for cookies on your website! To further avail quick and accurate scanning of your website, connect with CookieYes, in just a click! By continuing, you agree to CookieYes\'s <a href="%s" target="_blank">Privacy Policy</a> & <a href="%s" target="_blank">Terms of service</a>.', 'webtoffee-gdpr-cookie-consent' ), array(  'a' => array( 'href' => array(), 'target' => array() ) ) ), esc_url( $ckyes_link ), esc_url( $ckyes_privacy_policy ), esc_url( $ckyes_terms_conditions ) ).'</p></div>';
		} else {
			$notice .= '<p>'.__('Scan your website for cookies automatically with CookieYes!','webtoffee-gdpr-cookie-consent').'</p>';
			$notice .= '<p>'.sprintf( wp_kses( __( 'Connect with <a href="%s" target="_blank">CookieYes</a>, our scanning solution to get fast and accurate cookie scanning. By continuing, you agree to CookieYes\'s <a href="%s" target="_blank">Privacy Policy</a> & <a href="%s" target="_blank">Terms of service</a>.', 'webtoffee-gdpr-cookie-consent' ), array(  'a' => array( 'href' => array(), 'target' => array() ) ) ), esc_url( $ckyes_link ), esc_url( $ckyes_privacy_policy ), esc_url( $ckyes_terms_conditions ) ).'</p></div>';
			$notice .= '<a id="wt-cli-ckyes-connect-scan" class="button-primary pull-right">'.__('Connect & scan','webtoffee-gdpr-cookie-consent').'</a>';
		}
		
		return $notice;
	}
	public function get_last_scan_info() {

		$last_scan 		= 	$this->get_last_scan();
		$scan_btn_id	=	'wt-cli-ckyes-scan';
		$scan_btn_text	=	__('Start scan','webtoffee-gdpr-cookie-consent');
		
		if( $this->get_cookieyes_status() === 0 || $this->get_cookieyes_status() === false ) { // Disconnected with Cookieyes after registering account
			$scan_btn_id	=	'wt-cli-ckyes-connect-scan';
			$scan_btn_text	=	__('Connect & scan','webtoffee-gdpr-cookie-consent');
		}
		$scan_notice  =  $this->get_scan_default_html();
		
		if( $last_scan )
		{	
			$scan_status 	=  intval( ( isset( $last_scan['status'] ) ? $last_scan['status'] : 0 ) );
			if( $scan_status === 2 ){	
				$scan_notice  =  $this->get_scan_success_html( $last_scan );

			} else if( $scan_status === 4 ) {
				$scan_notice  =  $this->get_scan_failed_html( $last_scan );
			}
		}
		$notice   =	'<div class="wt-cli-cookie-scan-container">';
		$notice	 .= $scan_notice;
		$notice  .= '<div class="wt-cli-cookie-scanner-actions">'.apply_filters('wt_cli_ckyes_account_widget','').'<a id="'.$scan_btn_id.'" class="button-primary pull-right">'.$scan_btn_text.'</a></div>';
		$notice  .=	'</div>';
		return $notice;
		
	}
	public function get_scan_default_html(){
		$scan_notice	=	'<div class="wt-cli-callout wt-cli-callout-info"><p>'.__('You haven\'t performed a site scan yet.','webtoffee-gdpr-cookie-consent').'</p></div>';
		return $scan_notice;
	}
	public function get_scan_failed_html( $scan_data ){
		$last_scan_date =  ( isset( $scan_data['created_at'] ) ? $scan_data['created_at'] : '' );
		if( !empty(( $last_scan_date ))) {
			$last_scan_date  = date( 'F j, Y g:i a T',$last_scan_date );
		}
		$scan_notice	=	'<div class="wt-cli-callout wt-cli-callout-warning">';
		$scan_notice   .=	'<div class="wt-cli-scan-status"><p><b>'.__('Scan failed','webtoffee-gdpr-cookie-consent').'</b></p></div>';
		$scan_notice   .=	'<div class="wt-cli-scan-date"><p style="color: #80827f;">'.__("Last scan:","webtoffee-gdpr-cookie-consent").' '.$last_scan_date.'</p></div>';
		$scan_notice   .=	'</div>';
		return $scan_notice;
	}
	public function get_scan_success_html( $scan_data ){

		$scan_notice 	=  '';
		$result_page 	=  admin_url('edit.php?post_type='.CLI_POST_TYPE.'&page=cookie-law-info-cookie-scaner&scan_result');
		$last_scan_date =  ( isset( $scan_data['created_at'] ) ? $scan_data['created_at'] : '' );
		if( !empty(( $last_scan_date ))) {
			$last_scan_date  = date( 'F j, Y g:i a T',$last_scan_date );
		}
		$last_scan_text =  sprintf( wp_kses( __( 'Last scan: %s <a href="%s">View result</a>', 'webtoffee-gdpr-cookie-consent' ), array(  'a' => array( 'href' => array() ) ) ),$last_scan_date, esc_url( $result_page ) );
		$scan_notice	=	'<div class="wt-cli-callout wt-cli-callout-success">';
		$scan_notice   .=	'<div class="wt-cli-scan-status"><p><b>'.__('Scan complete','webtoffee-gdpr-cookie-consent').'</b></p></div>';
		$scan_notice   .=	'<div class="wt-cli-scan-date"><p style="color: #80827f;">'.$last_scan_text.'</p></div>';
		$scan_notice   .=	'</div>';

		return $scan_notice;

	}
	public function get_scan_action( $scan_btn_id = '', $scan_btn_text = '')  {
		return '<a id="'.$scan_btn_id.'" class="button-primary pull-right cli_scan_now">'.$scan_btn_text.'</a>';
	}
	public function get_scan_status_html(){
		$last_scan  	=	$this->get_last_scan();
		$total_urls		=	( isset( $last_scan['total_url'] ) ? $last_scan['total_url'] : 0 );
		$last_scan_date =   ( isset( $last_scan['created_at'] ) ? $last_scan['created_at'] : '' );
		$scan_estimate	=	gmdate("H:i:s", $this->get_current_scan_estimate());
		if( !empty(( $last_scan_date ))) {
			$last_scan_date  = date( 'F j, Y g:i a T',$last_scan_date );
		}
		
		$html	=	'<div class="wt-cli-scan-status-bar" style="display:flex;align-items:center; color: #2fab10;">
						<span class="wt-cli-status-icon wt-cli-status-success"></span><span style="margin-left:10px">'.__('Scan initiated...','webtoffee-gdpr-cookie-consent').'</span>
					</div>
					<div class="wt-scan-status-info">
						<div class="wt-cli-row">
							<div class="wt-cli-col-5">
								<div class="wt-scan-status-info-item">
									<div class="wt-cli-row">
										<div class="wt-cli-col-5">
											<b>'.__('Scan started at','webtoffee-gdpr-cookie-consent').':</b> 
										</div>
										<div class="wt-cli-col-7">'.$last_scan_date.'</div>
									</div>
								</div>
								<div class="wt-scan-status-info-item">
									<div class="wt-cli-row">
										<div class="wt-cli-col-5">
											<b>'.__('Total URLs','webtoffee-gdpr-cookie-consent').':</b> 
										</div>
										<div class="wt-cli-col-7">'.$total_urls.'</div>
									</div>
								</div>
								<div class="wt-scan-status-info-item">
									<div class="wt-cli-row">
										<div class="wt-cli-col-5">
											<b>'.__('Total estimated time (Approx)','webtoffee-gdpr-cookie-consent').':</b> 
										</div>
										<div class="wt-cli-col-7">'.$scan_estimate.'</div>
									</div>	
								</div>
							</div>
						</div>
					</div>
					<div class="wt-cli-notice wt-cli-info">'.__('Your website is currently being scanned for cookies. This might take from a few minutes to a few hours, depending on your website speed and the number of pages to be scanned.','webtoffee-gdpr-cookie-consent').
					
					'</br><b>'.__('Once the scanning is complete, we will notify you by email.','webtoffee-gdpr-cookie-consent').'</b></div>
					';
		return $html;
	}
	protected function get_ckyes_scan_id(){
		if( ! $this->ckyes_scan_id ) {
			$scan_id_trans = get_transient('wt_cli_ckyes_current_scan_id');
			$this->ckyes_scan_id = ( isset( $scan_id_trans ) ? $scan_id_trans : 0 );
		}
		return $this->ckyes_scan_id;
	}
	protected function get_scan_token(){
		if( ! $this->ckyes_scan_token ) {
			$scan_token_trans = get_transient('wt_cli_ckyes_scan_token');
			$this->ckyes_scan_token = isset( $scan_token_trans ) ? $scan_token_trans : '' ;
		}
		return $this->ckyes_scan_token;
	}
	public function set_scan_estimate( $time_in_seconds = 0 ){
		set_transient('wt_cli_ckyes_current_scan_estimate',intval( $time_in_seconds ), 2 * DAY_IN_SECONDS);
	}
	public function get_current_scan_estimate(){
		$time = intval( get_transient('wt_cli_ckyes_current_scan_estimate') );
		return $time;
	}
	protected function reset_scan_token(){
		set_transient('wt_cli_ckyes_current_scan_id',0);
		set_transient('wt_cli_ckyes_scan_token','');
	}
	/**
	* Check whether a scanning has initiated or not
	*
	* @since  2.3.3
	* @access public
	* @return bool
	*/
	public function check_scan_status(  ){
		$last_scan 		= 	$this->get_last_scan();
		$status			=	( isset( $last_scan['status'] ) ? $last_scan['status'] : 0 );
		if( $this->get_cookieyes_status() === 0 || $this->get_cookieyes_status() === false ) { 
			$status = 0;
		}
		return intval( $status );
	}
	
	public function fetch_scan_result( $request ){
		
		if( isset( $request) && is_object( $request )) {
			$request_body = $request->get_body();
			if( !empty( $request_body )) {
				if( is_wp_error( $this->save_cookie_data( json_decode( $request_body, true ) ) ) ) {
					wp_send_json_error( __('Token mismatch','webtoffee-gdpr-cookie-consent') );
				}
				wp_send_json_success( __('Successfully inserted','webtoffee-gdpr-cookie-consent') );
			}
		}
		wp_send_json_error( __('Failed to insert','webtoffee-gdpr-cookie-consent') );
	}

	public function save_cookie_data( $cookie_data ){

		global $wpdb;
		$url_table		=	$wpdb->prefix.$this->url_tb;
		$scan_id		=	$this->get_last_scan_id();
		$scan_urls		=	array();
		
		if( $cookie_data ) {
			if( $scan_id !== false ) {

				$sql	=	"SELECT id_cli_cookie_scan_url,url FROM `$url_table` WHERE id_cli_cookie_scan=$scan_id ORDER BY id_cli_cookie_scan_url ASC"; 
				$urls	=	$wpdb->get_results( $sql,ARRAY_A) ;
				foreach( $urls as $url_data ) {	
					$scan_urls[ $url_data['url']] = $url_data['id_cli_cookie_scan_url'];
				}
				$scan_data  		=	( isset( $cookie_data['scan_result'] ) ? json_decode( $cookie_data['scan_result'], true ) : array() );
				$scan_result_token  =	( isset( $cookie_data['scan_result_token'] ) ? $cookie_data['scan_result_token'] : array() );
				
				if( $this->validate_scan_instance( $scan_result_token ) === false ) {
					return  new WP_Error( 'invalid', __( 'Invalid scan token', 'webtoffee-gdpr-cookie-consent' ) );
				}
				$this->insert_categories( $scan_data );
				foreach( $scan_data as $key => $data ) {
					$cookies 	=	( isset( $data['cookies'] ) ? $data['cookies'] : '' );
					$category 	=	( isset( $data['category'] ) ? $data['category'] : '' );
					$this->insert_cookies($scan_id,$scan_urls,$cookies,$category);
				}
			}
		}
		$this->finish_scan( $scan_id );
	}
	public function finish_scan( $scan_id ){
		$scan_data	=	array(
			'current_action'	=>	'scan_pages',
			'current_offset'	=> 	-1,
			'status'			=>	2
		);
		$this->updateScanEntry( $scan_data, $scan_id );
		$this->reset_scan_token();
	}

	public function validate_scan_instance( $instance ){
		$last_instance = $this->get_last_scan_instance();
		if( $instance === $last_instance ) {
			return true;
		}
		return false;
	}
	
	public function get_last_scan_id(){
		$last_scan 		= 	$this->get_last_scan();
		$scan_id		=	( isset( $last_scan['id_cli_cookie_scan'] ) ? $last_scan['id_cli_cookie_scan'] : false );
		return $scan_id;
	}

	protected function insert_categories( $categories )
	{
		global $wpdb;
		$cat_table	=	$wpdb->prefix.$this->category_table;	
		$cat_arr	=	array();
		$cat_sql	=	"INSERT IGNORE INTO `$cat_table` (`cli_cookie_category_name`,`cli_cookie_category_description`) VALUES ";

		foreach($categories as $id => $category)
		{	
			$category		=	( isset( $category['category'] ) ? sanitize_text_field( $category['category'] ) : '' );
			$description	=	( isset( $category['category_desc'] ) ? addslashes( sanitize_textarea_field( $category['category_desc'] ) ) : '' );  ;
			
			if( !empty( $category )) {
				$cat_arr[]		=	"('$category','$description')";
			}
		}
		$cat_sql	=	$cat_sql.implode(",",$cat_arr);
		$wpdb->query( $cat_sql );
	}

	protected function insert_cookies($scan_id,$urls,$cookie_data,$category )
	{
		global $wpdb;
		$cookie_table	=	$wpdb->prefix.$this->cookies_tb;
        $category_table =	$wpdb->prefix.$this->category_table;
        
		$sql            =   "INSERT IGNORE INTO `$cookie_table` (`id_cli_cookie_scan`,`id_cli_cookie_scan_url`,`cookie_id`,`expiry`,`type`,`category`,`category_id`,`description`) VALUES ";
        
		$sql_arr    =   array();
		
		foreach( $cookie_data as $cookies )
		{
            $cookie_id      =   isset( $cookies['cookie_id'] ) ? esc_sql( sanitize_text_field( $cookies['cookie_id']) ) : '' ;
            $description    =   isset( $cookies['description'] ) ? esc_sql( sanitize_textarea_field( $cookies['description'] ) ) : '';
			$expiry         =   isset( $cookies['duration'] ) ? esc_sql( sanitize_text_field( $cookies['duration'] ) ) : '' ;
            $type           =   isset( $cookies['type'] ) ? esc_sql( sanitize_text_field( $cookies['type'] ) ) : '' ;
			
			$url_id			=	( isset( $cookies['frist_found_url'] ) ? $cookies['frist_found_url'] : '' );
			$url_id 		=	( isset( $urls[ $url_id ] ) ? $urls[ $url_id ] : 1 );
			$category_id    =   $wpdb->get_var("SELECT `id_cli_cookie_category` FROM `$category_table` WHERE `cli_cookie_category_name` = '$category'");
			$sql_arr[]      =   "('$scan_id','$url_id','$cookie_id','$expiry','$type','$category','$category_id','$description')";
		}
		$sql = $sql.implode(",",$sql_arr);
		$wpdb->query($sql);
	}
	
}
new Cookie_Law_Info_Cookie_Scaner();